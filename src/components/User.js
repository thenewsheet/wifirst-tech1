import React from 'react';

class User extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user : this.findUser()
		}
	}
	
	componentDidUpdate(prevProps) {
		//wait for props.user loading
		if (this.props.users.length && !prevProps.users.length) {
			const user = this.findUser();
			this.setState({user});
		}
	}
	
	findUser() {
		if(!this.props.users.length) return false;
		const userId = parseInt(this.props.match.params.id);
		return this.props.users.find(e => e.id === userId);
	}
	
	address() {
		const user = this.state.user;
		let address = [];
		if(!user.address) return '';
		['street', 'suite', 'city'].forEach(k => address.push(user.address[k]));
		return address.join(', ') + (user.address.zipcode ? ' - ' + user.address.zipcode : '');
	}
	
	website() {
		const user = this.state.user;
		if(user.website) return <a href={"//" + user.website} target="_blank" rel="noopener noreferrer">{user.website}</a>;
		return '';
	}
	
	company() {
		const user = this.state.user;
		return user.company && user.company.name;
	}
	
	email() {
		const user = this.state.user;
		if(user.email) return <a href={"mailto:" + user.email} >{user.email}</a>;
		return '';
	}
	
	render() {
		if(!this.props.users.length) return <h1>Loading...</h1>;
		if(this.state.user === undefined) return <h1>User not found !</h1>;
		
		return (
			<div className="border1">
				<h1>{this.state.user.name}</h1>
				<ol className="squareList">
					{
						['username', 'email', 'address', 'phone', 'website', 'company'].map( (e,i) => {
							return <li key={i}>{e} : {typeof this[e] === 'function' ? this[e]() : this.state.user[e] }</li>;
						})
					}
				</ol>
			</div>
		);
	}
} 


export default User; 
