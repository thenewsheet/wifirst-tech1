import React from 'react';

class PageTitle extends React.Component {
	render() {
		const path = this.props.location.pathname;
		
		const pages = {
			'^/post/\\d+' : 'Post',
			'^/$' : 'Liste des posts',
			'^/user/\\d+' : 'Informations utilisateur'
		};
		
		const title = Object.keys(pages).find( r => path.match(new RegExp(r)) );
		return (title && pages[title]) || '';
	}
}

export default PageTitle;
