import React from 'react';
import { Link } from 'react-router-dom';
import {Row, Col} from 'react-bootstrap';

import API from '../api/api';

class PostsList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			posts : [],
			error : null
		};
	}
	
	componentDidMount() {
		API.posts().then( posts => {
			this.setState({
				posts
			});
		}).catch(e => {
			console.log(e);
			this.setState({error:true});
		});
	}
	
	userName(userId) {
		return this.props.users.find(e => e.id === userId).username;
	}
	
	render() {
		if(!this.state.posts.length || !this.props.users.length) return <h1>Loading...</h1>;
		
		return (
		<div>
			{
			this.state.posts.map(e => {
				return (
					<div key={e.id} className="border1">
						<Row>
							<Col><Link to={"/post/"+e.id}><h2>{e.title}</h2></Link></Col>
						</Row>
						<Row>
							<Col>Par : <Link to={"/user/"+e.userId}>{this.userName(e.userId)}</Link></Col>
						</Row>
					</div>
				);
			})
			}
		</div>);
	}
} 


export default PostsList;
