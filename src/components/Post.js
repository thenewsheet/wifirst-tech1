import React from 'react';
import { Link } from 'react-router-dom';

import Comments from './Comments'

import API from '../api/api';

class Post extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			postContent : null,
			error : false
		}
	}
	
	userName() {
		if(!this.state.postContent || !this.props.users.length) return '';
		const user = this.props.users.find(e => e.id === this.state.postContent.userId);
		return user.name;
	}
	
	componentDidMount() {
		const postId = this.props.match.params.id;
		API.post(postId).then(postContent => {
			if(postContent && !Object.keys(postContent).length) postContent = undefined;
			this.setState({
				postContent
			});
		}).catch(e => {
			console.log(e);
			this.setState({
				error : true
			});
		});
	}
	
	render() {
		if(this.state.error) return <h1>Une erreur est survenue</h1>;
		if(this.state.postContent === null) return <h1>Loading...</h1>;
		if(this.state.postContent === undefined) return <h1>404 - Not found</h1>;
		return (
			<div>
			<div className="border1">
				<h1>{this.state.postContent.title}</h1>
				<p>{this.state.postContent.body}</p>
				<p>Auteur : <Link to={"/user/"+this.state.postContent.userId}>{this.userName()}</Link></p>
			</div>
			<div className="border1">
				<h3>Commentaires</h3>
				<Comments postId={this.props.match.params.id} />
			</div>
			</div>
		);
	}
} 


export default Post; 
