import React from 'react';

import API from '../api/api'; 


class Comments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			comments : [],
			ready : false,
			error : false
		};
	}
	
	componentDidMount() {
		const postId = this.props.postId;
		API.comments(postId).then(comments => {
			this.setState({
				comments,
				ready : true
			});
		}).catch(e => {
			console.log(e);
			this.setState({
				error : true
			});
		});
	}
	
	render() {
		if(this.state.error) return <p>Une erreure est survenue</p>;
		if(!this.state.ready) return <p>Loading...</p>;
		if(!this.state.comments.length) return <p>Aucun commentaires</p>;
		return (
			this.state.comments.map( e => {
				return (
					<div key={e.id} className="border1">
						<p><i>{e.name}</i> ({e.email})</p>
						<p>{e.body}</p>
					</div>
				)
			})
		);
	}
}

export default Comments;
