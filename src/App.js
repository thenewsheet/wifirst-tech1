import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import {Row, Col} from "react-bootstrap";

import API from './api/api';

import PostsList from './components/PostsList';
import Post from './components/Post';
import User from './components/User';
import PageTitle from './components/PageTitle';

import './assets/bootstrap.min.css';
import './assets/main.css'

class App extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			users : []
		}
	}
	
	componentDidMount() {
		API.users().then( users => {
			this.setState({
				users
			});
		}).catch(e => {
			console.log(e);
		});
	}
	
	render() {
		return (
			<div className="app">
			<Router>
				<Row>
					<Col xs lg="2"><Link to="/">Home</Link></Col>
					<Col md="auto"><Route path="/" component={PageTitle}/></Col>
				</Row>
				<Row>
				<Col>
					<Route exact path="/" render={(props) => <PostsList {...props} users={this.state.users} />}/>
					<Route path="/post/:id" render={(props) => <Post {...props} users={this.state.users} />}/>
					<Route path="/user/:id" render={(props) => <User {...props} users={this.state.users} />}/>
				</Col>
				</Row>
			</Router>
			</div>
		);
	}
}


export default App;
