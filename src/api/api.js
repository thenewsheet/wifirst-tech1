 
const urls = {
	posts : "https://jsonplaceholder.typicode.com/posts",
	post : "https://jsonplaceholder.typicode.com/posts/",
	comments : "https://jsonplaceholder.typicode.com/comments",
	users : "https://jsonplaceholder.typicode.com/users"
};


class API {
	static posts() { return fetch(urls.posts).then(function(response) {return response.json();}); }
	static post(postId) { return fetch(urls.post + postId).then(function(response) {return response.json();});}
	static comments(postId) { return fetch(urls.comments + '?postId=' + postId).then(function(response) {return response.json();})}
	static users() { return fetch(urls.users).then(function(response) {return response.json();});}
};

export default API;
